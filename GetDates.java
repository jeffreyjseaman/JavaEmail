
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.time.LocalTime;


public class Main {

	public static void main(String[] args) throws ParseException {
		Date myDate = new Date();


	    SimpleDateFormat mdyFormat = new SimpleDateFormat("MM/dd/yyyy");

	    // Format the date to Strings
	    String mdy = mdyFormat.format(myDate);


	    // Results...
	    System.out.println(mdy);

	   
	    LocalTime mytime = LocalTime.of(22, 15);
	    System.out.println(mytime);
	      
	}

}
