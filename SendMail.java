import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMail {

	public static void main(String[] args) {

		String CorrectAnswers = null;
		String WrongAnswers = null;
		
		sendEmail(CorrectAnswers,WrongAnswers);
	}
	
	public static void sendEmail(String CorrectAnswers, String WrongAnswers)
	{
		final String username = "lauren.genter@live.com";
		final String password = "";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.live.com");
		props.put("mail.smtp.port", "25");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("colorwheel@ccac.edu"));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse("lauren.genter@live.com"));
			message.setSubject("Color Wheel Results");
			message.setText("Dear CCAC Student,"
				+ "\n\n No spam to my email, please!");

			Transport.send(message);

			System.out.println("Successfully Sent");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
}